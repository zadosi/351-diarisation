%%

[Z_audio, fs] = audioread('zach_alone.wav');
% windowing for the audio file and converting to the frequency domain
window = hann(1024, "periodic");
S = stft(Z_audio,"Window",window,"OverlapLength",512,"Centered",false); 
% extract cepstral coefficients
Z_coeffs = mfcc(S,fs);

[J_audio, fs] = audioread('john_alone.wav');
% windowing for the audio file and converting to the frequency domain
window = hann(1024, "periodic");
S = stft(J_audio,"Window",window,"OverlapLength",512,"Centered",false); 
% extract cepstral coefficients
J_coeffs = mfcc(S,fs);

[G_audio, fs] = audioread('grant_alone.wav');
% windowing for the audio file and converting to the frequency domain
window = hann(1024, "periodic");
S = stft(G_audio,"Window",window,"OverlapLength",512,"Centered",false); 
% extract cepstral coefficients
G_coeffs = mfcc(S,fs);

[T_audio, fs] = audioread('tommy_alone.wav');
% windowing for the audio file and converting to the frequency domain
window = hann(1024, "periodic");
S = stft(T_audio,"Window",window,"OverlapLength",512,"Centered",false); 
% extract cepstral coefficients
T_coeffs = mfcc(S,fs);

%%
% looking to train computer with the MFCCs of each of our voices
% also look into getting the pitch of each files and looking at pitch
% contour of each speaker to characterizeZ_coeffs
nbins = 60;

figure
subplot(2,2,1)
stem(Z_coeffs), title("Zach's Coefficient Stem")
Z_coeff_avgs = mean(Z_coeffs, 1);

subplot(2,2,2)
stem(J_coeffs), title("John's Coefficient Stem")
J_coeff_avgs = mean(J_coeffs, 1);

subplot(2,2,3)
stem(G_coeffs), title("Grants's Coefficient Stem")
G_coeff_avgs = mean(G_coeffs, 1);

subplot(2,2,4)
stem(T_coeffs), title("Tommy's Coefficient Stem")
T_coeff_avgs = mean(T_coeffs, 1);

% Zach's MFCCs with transform
figure
for i = 1:size(Z_coeffs,2)
    subplot(2,7,i) % all coefficients on one figure now, separate plots
    %histogram(Z_coeffs(:,i),nbins,"Normalization","pdf")
    histogram(Z_coeffs(:,i),nbins)
    title(sprintf("Zach's Coefficient %d",i-1))
end

% Zach's MFCCs with no transform
Z_coeffs = mfcc(Z_audio,fs);
figure
for i = 1:size(Z_coeffs,2)
    subplot(2,7,i) % all coefficients on one figure now, separate plots
    histogram(Z_coeffs(:,i),nbins,"Normalization","pdf")
    title(sprintf("Zach's non-transform Coefficient %d",i-1))
end

% Johns's MFCCs
figure
for i = 1:size(J_coeffs,2)
    subplot(2,7,i) % all coefficients on one figure now, separate plots
    histogram(J_coeffs(:,i),nbins,"Normalization","pdf")
    title(sprintf("John's Coefficient %d",i-1))
end

% Grant's MFCCs
figure
for i = 1:size(G_coeffs,2)
    subplot(2,7,i) % all coefficients on one figure now, separate plots
    histogram(G_coeffs(:,i),nbins,"Normalization","pdf")
    title(sprintf("Grant's Coefficient %d",i-1))
end

% Tommy's MFCCs
figure
for i = 1:size(T_coeffs,2)
    subplot(2,7,i) % all coefficients on one figure now, separate plots
    histogram(T_coeffs(:,i),nbins,"Normalization","pdf")
    title(sprintf("Tommy's Coefficient %d",i-1))
end
%%

% just testing a plot of the output
% going to add pitch contour to each plot of the individual time plots

% none of the pitch seems to be working, might need to change the pD
% values? also look into what the pD = audiopluginexample.SpeechPitchDetector;
% line is doing

pD = audiopluginexample.SpeechPitchDetector;

figure


% Zach time and pitch contour
dt = 1/fs;
t_Z = 0:dt:(length(Z_audio)*dt)-dt;
subplot(2,4,1),
plot(t_Z,Z_audio); xlabel('Seconds'); ylabel('Amplitude'); title("Zach (t)")

%[~,Z_pitch] = process(pD,Z_audio);
subplot(2,4,5),
%plot(t_Z,Z_pitch); xlabel('Seconds'); ylabel('Amplitude'); title("Zach's Pitch")
t_Z = transpose(t_Z);
Z_pitch = pitch(Z_audio, fs);
plot(Z_pitch);

% John time and pitch contour
dt = 1/fs;
t_J = 0:dt:(length(J_audio)*dt)-dt;
subplot(2,4,2),
plot(t_J,J_audio); xlabel('Seconds'); ylabel('Amplitude'); title("John (t)")

%[~,J_pitch] = process(pD,J_audio);
subplot(2,4,6),
%plot(t_J,J_pitch); xlabel('Seconds'); ylabel('Amplitude'); title("John's Pitch")
t_J = transpose(t_J);
J_pitch = pitch(J_audio, fs);
plot(J_pitch);

% Grant time and pitch contour
dt = 1/fs;
t_G = 0:dt:(length(G_audio)*dt)-dt;
subplot(2,4,3),
plot(t_G,G_audio); xlabel('Seconds'); ylabel('Amplitude'); title("Grant (t)")

%[~,G_pitch] = process(pD,G_audio);
subplot(2,4,7),
%plot(t_G,G_pitch); xlabel('Seconds'); ylabel('Amplitude'); title("Grant's Pitch")
t_G = transpose(t_G);
G_pitch = pitch(G_audio, fs);
plot(G_pitch);

% Tommy time and pitch contour
dt = 1/fs;
t_T = 0:dt:(length(T_audio)*dt)-dt;
subplot(2,4,4),
plot(t_T,T_audio); xlabel('Seconds'); ylabel('Amplitude'); title("Tommy (t)")

%[~,T_pitch] = process(pD,T_audio);
subplot(2,4,8),
%plot(t_T,T_pitch); xlabel('Seconds'); ylabel('Amplitude'); title("Tommy's Pitch")
t_T = transpose(t_T);
T_pitch = pitch(T_audio, fs);
plot(T_pitch);