function [voiced_flag] = detectVoicedFrame(audio_read,fs, frameTime,... 
hopPercentage, pwrThreshold, freqThreshold)
%DETECTVOICEDFRAME Voiced Frame Detector
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% detect the voiced frames, marked as 1, otherewise 0
% -------------- Parameters Explanation ----------------
% audio_read and fs: get from `audioread
% frameTime: frame length in s
% hopPercentage: each frame jump how much of the frame
% pwrThreshod: Frames with power below this threshold (in dB) are likely to
%               be silence
% Frames with zero crossing rate above this threshold (in Hz) are likely to
%               be silence or unvoiced speech
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
samplesPerFrame = floor(frameTime*fs);
startIdx = 1;
stopIdx = samplesPerFrame;
increment = floor(hopPercentage/100*samplesPerFrame);
numFrames = floor((length(audio_read) - samplesPerFrame)/increment) + 1;

voiced_flag = zeros(numFrames, 1);

for i = 1: numFrames

    xFrame = audio_read(startIdx:stopIdx,1); % 30ms frame
    
    try

    if audiopluginexample.SpeechPitchDetector.isVoicedSpeech(xFrame,fs,... % Determining if the frame is voiced speech
            pwrThreshold,freqThreshold)
        voiced_flag(i) = 1;
    end
    
    catch 
       disp('Please install Audio Toolbox'); 
    end
    startIdx = startIdx + increment;
    stopIdx = stopIdx + increment;
end

end

