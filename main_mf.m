trainName = {'Tommy.wav','Lauren.wav'};

%% 1 Feature Extraction
% Extract each piece's feature (MFCC and pitch)

feat = {};
for i = 1:length(trainName)
    feat{i, 1} = builtFeatureExtractor(trainName{i}, i);
end


%% 2 Machine Learning process to get the overall feature

unknown = builtFeatureExtractor('together.WAV', -1);
tinfo = [feat{1}; feat{2}];

%Seeing which k value returns the best result
result3 = [];
for i = 1:size(unknown,1)
    result3(i) = nearestneighbor(unknown(i, :), 3, tinfo);
end

result4 = [];
for i = 1:size(unknown,1)
    result4(i) = nearestneighbor(unknown(i, :), 4, tinfo);
end

result5 = [];
for i = 1:size(unknown,1)
    result5(i) = nearestneighbor(unknown(i, :), 5, tinfo);
end


%% 3 Smoothing and Noise Removal
% Extract each slice of speech's feature. 

%vec: moving window to average and match
vec = zeros(1, 150);

%vec2: averaged outcome (allocating space here)
vec2 = zeros(size(result3));

for i = 1:length(result3)
    vec = [vec(2:end) result3(i)]; %left shifting the data to be examined
    nonzeroavg = sum(vec)/nnz(vec); %finding the average, not including the noiseless entries
    if(abs(2-nonzeroavg) > abs(1-nonzeroavg)) %fitting the average to the label
        vec2(i) = 1;
    else
        vec2(i) = 2;
    end

end


%% 4 Showing Result
% output a chart, showing the matches

figure(1)
plot(result3, '.');
title('K = 3');
figure(2)
plot(result4, '.');
title('K = 4');
figure(3)
plot(result5, '.');
title('K = 5');

figure(4)
plot(vec2, '.');