clear
clc

%% DIARIZATION with self-made MFCC extractor
%{

EECS 351, FA19, University of Michigan
Junhao Cao, Thomas Cope, Grant Dukus, Thomas Cope
Last edited: 9 December 2019

Goal: to perform a simple 'who spoke when' diarization with supervised
learning

Inputs: training files of each subject speaking clearly, a file to be tested

Outputs: a plot displaying 'who spoke when,' as well as a text file (TBD)

Directions:
Please enter the training files (as .wav) for each subject in section 0. Then, enter the
file to be investigated (also as .wav) in section 2. 

%}



%% 0 Pre-working

trainName = {'TrainTommy.WAV'; 'TrainZach.WAV'; 'TrainGrant.WAV'; 'TrainJohn.WAV'};

%% 1 Feature Extraction
% Extract each piece's feature (MFCC and pitch)

feat = {};
for i = 1:length(trainName)
    feat{i, 1} = featureExtractor(trainName{i}, i);
end

%% 2 Machine Learning process to get the overall feature

%File to be analyzed:
tester = 'ShortConference.WAV';
[aud, ~] = audioread(tester);

[unknown] = featureExtractor(tester, -1);
tinfo = [feat{1}; feat{2}; feat{3}; feat{4}];

%Using a kNN algorithm with k = 3
result3 = [];
for i = 1:size(unknown,1)
    result3(i) = nearestneighbor(unknown(i, :), 3, tinfo);
end


%% 3 Conference 'Blank' Indentification and Slicing
% Slice the conference into pieces by identifying the blank length

[div_cell, begin, end_n, fs] = divideAudio(tester, 0.01, 0.2, 1);
begin(1) = begin(1) + floor((.75*fs));


%% 4 Slice Feature Extraction and Matching
% Extract each slice of speech's feature, as well as match them to the most likely speaker. 
% Due to an inaccuracy, we are only utilizing the indices. However the idea
% is the same as in main_v4.m


%dividing by 504 to convert to frame specific
numslices = length(begin);
match = [];
for i = 1:numslices
    
    index1 = floor(begin(i)/504);
    
    index2 = floor(end_n(i)/504);
    
    if(index2>size(aud,1)) 
        index2 = size(aud,1); 
    end
    
    match(i) = mode(nonzeros(result3(index1:index2)));

end

 


%% 5 Showing Result
% output a chart, showing the likely speaker

figure(1)
plot(result3, '.');
title('General Shape (no slicing) with K = 3');
ylim([0 5]);

figure(2)
scatter(1:length(match), match, 50, 'filled');
ylim([0 5]);
title('Matches for each utterance');

numcol = 1:length(begin);

figure(3)
input = [numcol' begin' end_n' match'];
plotter(input, aud, fs, tester);
