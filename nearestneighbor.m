function [match] = nearestneighbor(input, k, traininginfoWnames)

    %load(traininginfoWnames.mat);
    %->training info should be in the format:
    % [pitch, mfcc1, mfcc2, ..., mfcc13, 'name']
    
    % remove the label
    input = input(1:end-1);
    traininginfo = traininginfoWnames(:, 1:end-1);
    
    distance = zeros(1, size(traininginfo, 1)); %empty vec to be filled in with each distance
    for i = 1: size(traininginfo,1) % for each row of training data
        
        distance(i) = sqrt(sum((input-traininginfo(i, :)).^2)); %euclidian distance 
        
    end
    
    [~, index] = mink(distance, k); %index is rows from training info
    
    names = zeros(1, k); % empty vec to be filled with the names
    
    for i = 1:k
        
        names(i) = traininginfoWnames(index(i), end);
        
    end
    
    match = mode(names);
    
end

