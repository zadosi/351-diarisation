function [match] = matcher(input,test)

[unknown] = builtFeatureExtractor(input, -1);

result = [];
for i = 1:size(unknown,1)
    result(i) = nearestneighbor(unknown(i, :), 3, test);
end

% vec = zeros(1, 50);
% 
% %vec2: averaged outcome (allocating space here)
% vec2 = zeros(size(result));
% 
% for i = 1:length(result)
%     vec = [vec(2:end) result(i)]; %left shifting the data to be examined
%     nonzeroavg = sum(vec)/nnz(vec); %finding the average, not including the noiseless entries
%     temp = abs([1 2 3 4] - nonzeroavg);
%     [~,vec2(i)] = min(temp);
% 
% end

result = nonzeros(result);
% 
match = mode(result);
% match = result;

end

