# Goals:

Design a conference speaker detection system. By inputting one standard sample from each person from a group and a conference record, the system can identify the people who are talking during the conference. 

## Something needs to clarify in this Goal:
- Conference: it means that there will be no overlaps for the voice, i.e, there will be no ‘quarrels’, like other people speaking during one’s talking. 
- Standard sample: it means a piece of voice that is able to take features of the speaker. 
- A group: it just means a limited number of people. 

# Subsystems:
## Pre-training System
REQUIRES: 	pieces of standard voice from different people
EFFECTS:	Take the features from the voice, and store them as a standard to compare. 
### Possible problems which need to be conquered:
- How can the features be taken out?
- How can the features be learned?
- How to make it enough accurate? 

### Possible background knowledge required:
- MFCC, pitch, DCT… about how to take the ‘raw’ feature
- Machine Learning… about how to refine the feature

## Dialog-Detecting System
REQUIRES: 	a piece of conference record by some people
EFFECTS:	Identify who is talking inside the conference dialog. 
### Possible problems which need to be conquered
- How can the system detect the feature through the record? 
- How frequently should the feature be taken out from the record? Too many may cause the overlap of the processing streamline. Or can this be prevented? 
### Possible knowledge background required:
- Machine Learning, Probability Theory?… about how to detect the features


# Implementation With files (steps): 
1. Download our training files and audio files from our website: https://sites.google.com/view/eecs351diarization/home
2. Clone the entire directory here in Gitlab
2. Open main_v4.m
3. Follow steps in main_v4.m

Note: we designed our own MFCC feature extractor, following the steps in our presentation and website. However, MATLAB's function was significantly
faster, so we decided to use that in main_v4.m. If you would like to see the function of our own MFCC feature extracter, please run main_alt.m

Enjoy!
