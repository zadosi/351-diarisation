clear
clc
close all
%% DIARIZATION
%{

EECS 351, FA19, University of Michigan
Junhao Cao, Thomas Cope, Grant Dukus, Thomas Cope
Last edited: 9 December 2019

Goal: to perform a simple 'who spoke when' diarization with supervised
learning

Inputs: training files of each subject speaking clearly, a file to be tested

Outputs: a plot displaying 'who spoke when,' as well as a text file (TBD)

Directions:
Please enter the training files (as .wav) for each subject in section 0. Then, enter the
file to be investigated (also as .wav) in section 2. 

%}



%% 0 Pre-working

trainName = {'TrainTommy.WAV'; 'TrainZach.WAV'; 'TrainGrant.WAV'; 'TrainJohn.WAV'};
trainLabel = {'tommy', 1; 'zach', 2; 'grant',3; 'john', 4};
%% 1 Feature Extraction
% Extract each piece's feature (MFCC and pitch)

feat = {};
for i = 1:length(trainName)
    feat{i, 1} = builtFeatureExtractor(trainName{i}, i);
    feat{i, 2} = trainName{i};
end

%% 2 Machine Learning process to get the overall feature

%File to be analyzed:
tester = input('Please type the tester audio filename\n', 's');
while ~exist(tester, 'file') && ~(tester == "")
    disp('the filename does not exist, try again')
    tester = input('Please type the tester audio filename\n', 's');
end
if tester == ""
    disp('Exit. Go Blue!')
    return
end
disp('file found. Processing.......');
tommy = 1; zach = 2; grant = 3; john = 4;
[aud, fs_con] = audioread(tester);
[unknown] = builtFeatureExtractor(tester, -1);
tinfo = [feat{tommy, 1}; feat{zach, 1}; feat{grant, 1}; feat{john, 1}];

%Using a kNN algorithm with k = 3
result3 = [];
for i = 1:size(unknown,1)
    result3(i) = nearestneighbor(unknown(i, :), 3, tinfo);
end


%% 3 Conference 'Blank' Indentification and Slicing
% Slice the conference into pieces by identifying the blank length

[div_cell, begin, end_n, fs] = divideAudio(tester, 0.01, 0.2, 1);


%% 4 Slice Feature Extraction and Matching
% Extract each slice of speech's feature, as well as match them to the most likely speaker. 

numslices = length(begin);
match = [];
for i = 1:numslices
% insert "ShortConference.WAV" or "LongConference.WAV" for the last parameter
    input_name = strcat('test', int2str(i), '.WAV'); 
    match(i) = matcher(input_name,tinfo);
end



%% 5 Showing Result
% output a chart, showing the likely speaker

figure(1)
plot(result3, '.');
title('General Shape (no slicing) with K = 3');
ylim([0 5]);

figure(2)
scatter(1:length(match), match, 50, 'filled');
ylim([0 5]);
title('Matches for each utterance');

numcol = 1:length(begin);

figure(3)
input_sth = [numcol' begin' end_n' match'];
plotter(input_sth, aud, fs, tester, trainName);

com_Spk = combineSpeakers(div_cell, match);

traverseSound(com_Spk, fs, trainLabel);

