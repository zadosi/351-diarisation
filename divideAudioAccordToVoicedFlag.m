function [audio_cell] = divideAudioAccordToVoicedFlag(audio_read,fs, frameTime, ...
voiced_flag, hopPercentage, tolerance, if_to_file)
%DIVIDE_AUDIO_ACCORD_TO_VOICED_FLAG divide the audio based on the flag
audio_cell = {};
samplesPerFrame = floor(frameTime*fs);
hop_step = floor(hopPercentage/100*samplesPerFrame);
edge_flag = detectEdge(voiced_flag, tolerance, 90, 10);
for i = 2:2:length(edge_flag)
    audio_cell{i/2} = audio_read(hop_step * edge_flag(i/2): hop_step * edge_flag(i/2 + 1));
end
if if_to_file ~= 0
    for i = 1: length(audio_cell)
        audiowrite(['test' num2str(i) '.wav'], audio_cell{1, i}, 16000)
    end
end
end

function [edge_flag_log] = detectEdge(voiced_flag, tolerance, percentage_voiced, percentage_slnced)
% DETECT EDGE: detect edge from the voiced_flag
% tolerance: for the range of 'tolerance' before and after the iterator, it
% should be clearly divided to a length of 'tolerance' 
% 。。。。。。。。。
%                   。。。。。。。。。
% |---tolerance---| |---tolerance---|
% percentage: for the two parts, some of the tolerance can be accepted. 
% this is the case for having percentage_voiced of 8/9 * 100 (%) and
% percentage_blank of 1/9 * 100 (%)
% 。。 。。。。。。          。
%     。            。。。。。 。。。
% |---tolerance---| |---tolerance---|

i = 1; 
it = tolerance + 1;
edge_flag_log = 0;
% dropping_edge = [ones(1, tolerance) zeros(1,tolerance)]'; 
% rising_edge = [zeros(1, tolerance) ones(1,tolerance)]';
threshold_for_pcnt_voiced = floor(tolerance*percentage_voiced/100);
threshold_for_pcnt_slnced = floor(tolerance*percentage_slnced/100);
if sum(voiced_flag(1:tolerance)) > floor(tolerance/2) % not begin with silence
    edge_flag_log(i) = 1;
    i = i + 1;
end
while(it <= length(voiced_flag) - tolerance) 
    
    if (sum(voiced_flag((it - tolerance):it)) > threshold_for_pcnt_voiced &&...
            sum(voiced_flag((it+1):(it + tolerance))) <= threshold_for_pcnt_slnced)|| ...   %% rising edge
       (sum(voiced_flag((it+1):(it + tolerance))) > threshold_for_pcnt_voiced &&...
            sum(voiced_flag((it - tolerance):it)) <= threshold_for_pcnt_slnced)            %% dropping edge
%         isequal(voiced_flag((it - tolerance):(it + tolerance - 1)),dropping_edge) || ...
%             isequal(voiced_flag((it - tolerance):(it + tolerance - 1)), rising_edge)
       edge_flag_log(i) = it;
       i = i + 1; 
    end
    it = it + 1; 
end
if mod(length(edge_flag_log), 2) ~= 0
    edge_flag_log(i) = it;
end
end


