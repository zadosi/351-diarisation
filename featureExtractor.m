function [fwl] = featureExtractor(audio_read, speakerLabel)
%Audioread
[input, fs] = audioread(audio_read);

pwrThreshold = -50; % Frames with power below this threshold (in dB) are likely to be silence
freqThreshold = 1000; % Frames with zero crossing rate above this threshold (in Hz) are likely to be silence or unvoiced speech

%Changing stereo files to audio files
[m, n] = size(input); %gives dimensions of array where n is the number of stereo channels
if n == 2
    y = input(:, 1) + input(:, 2); %sum(y, 2) also accomplishes this
    peakAmp = max(abs(y)); 
    y = y./peakAmp;
    %  check the L/R channels for orig. peak Amplitudes
    peakL = max(abs(audio_read(:, 1)));
    peakR = max(abs(audio_read(:, 2))); 
    maxPeak = max([peakL peakR]);
    %apply x's original peak amplitude to the normalized mono mixdown 
    y = y*maxPeak;
    input = y;   
end
frameTime = 21e-3; %changed so that 1009 samples per frame -> close to 1024, increases efficiency of fft()
samplesPerFrame = floor(frameTime*fs);
increment = floor(0.5*samplesPerFrame); %50 percent overlap
voicing = detectVoicedFrame(input, fs, frameTime, 50, pwrThreshold, freqThreshold);


%Pre-emphasis
a = 0.97;
input = input - a.*[0; input(1:end-1)];

%Framing
numframes = floor(length(input)/increment)-2;
frames = [];
for i = 1:numframes
    startframe = i*increment;
    frames(i,:) = input(startframe:startframe+samplesPerFrame);
end


%Windowing
window = hamming(1009);
% windowed_frames = frames.*window;

for i = 1:numframes
    frames(i, :) = frames(i,:).*window';
end

frames = frames';

%FFT and Power Calc
periodogram = ((abs(fft(frames, 1024))).^2)./numframes;

%Multiplication of Filter Banks and subsequent DCT
coeffs = zeros(13,numframes);

for i = 1:numframes
    coeffs(:,i) = dct(log10(filtmult(periodogram(:,i), fs)));
end

coeffs = coeffs';

voicing = voicing(1:end-1);

coeffs(voicing == 0,:) = 100;

label = ones(1, size(coeffs,1))'.*speakerLabel;
fwl = [coeffs label]; %insert the pitch in the last spot of each vector

    for i = 1:size(fwl,1)
        if((sum(coeffs(i,:)))>100)
            fwl(i, end) = 0;
        else
            fwl(i,1:end-1) = (coeffs(i,:) - mean(coeffs(i,:)))./std(coeffs(i,:));
        end
    end
   
end

