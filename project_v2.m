% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Text-independent Speech Indetification
%
% +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% 
% Audio files needed in this code:
% 'tommy_alone.wav'; 'zach_alone.wav'; 'grant_alone.wav'; 'john_alone.wav';
% 'Zach_test.wav'
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% put file names into stored memory, or the builtFeatureExtractor seems
% that it cannot read the temporary variable of filename. 
trainName = {'tommy_alone.wav'; 'zach_alone.wav'; 'grant_alone.wav'; 'john_alone.wav'};
[Tfeat]= builtFeatureExtractor(trainName{1, 1}, 1);
[Zfeat]= builtFeatureExtractor(trainName{2, 1}, 2);
[Gfeat]= builtFeatureExtractor(trainName{3, 1}, 3);
[Jfeat]= builtFeatureExtractor(trainName{4, 1}, 4);

% randomly choose some of the frames to see mfcc 
hold on
plot(Tfeat(176, :));
plot(Zfeat(1, :));
plot(Gfeat(130,:));
plot(Jfeat(176,:));
hold off

% trying to combine these things into a 'conference'. Tommy, Zach, John,
% Grant have speech one by one. 
traininginfoWithnames = [Tfeat; Zfeat; Gfeat; Jfeat];
% deleting all the parts which is zero energy (labeled in the
% builtFeatureExtractor)
traininginfoWithnames(any(isnan(traininginfoWithnames), 2), :) = []; 
% featureVectors: just MFCC
featureVectors = traininginfoWithnames(:,1:13);
% Normalise 
m = mean(featureVectors);
s = std(featureVectors);
traininginfoWithnames(:,1:13) = (featureVectors-m)./s;

% test: Zach

zach_test = 'Zach_test.wav';

[Unknown]= builtFeatureExtractor(zach_test, 0);
test = Unknown(:, 1:end-1);
test(any(isnan(test), 2), :) = [];

result = zeros(size(test,1), 1);

% 
for i = 1:size(test,1)
    result(i) = nearestneighbor(test(i, :), 3, traininginfoWithnames);
end

figure(2)
%plot(result);

histogram(result);

