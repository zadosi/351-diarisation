function [] = plotter(input,audio_s, fs, tester, speakers)
    [~, n] = size(audio_s);
   if n ==2
    	audio = stereo2mono(audio_s);
   else
       audio = audio_s;
   end
   snippet = input(:, 1);
   first = input(:, 2);
   last = input(:, 3);
   last(end) = length(audio);
   label = input(:, 4);
   
   colors = 'rmgbc';
   
   t = (0:length(audio)-1)./fs;
   hold on
   for i = 1:length(snippet)
       time = t((first(i)):(last(i)));
       result = ones(size(time)).*label(i);
       plot(time, result, colors(label(i)),'Linewidth', 50);
   end
   hold off
   grid;
   %set(gca,'YTick',[])
   ylim([0 5]);
   
   ylabel('Speaker');
   yticklabels({'noise','','Tommy','','Zach','','Grant','', 'John'})
   xlabel('time (s)');
   title(strcat('Diarization of', {' '}, tester), 'Interpreter', 'none');
   
%    legend('Speaker 1', 'Speaker 2', 'Speaker 3', 'Speaker 4', 'Location', 'Northwest');
   
    %[~, hobj, ~, ~] = legend({'Speaker 1', 'Speaker 2', 'Speaker 3', 'Speaker 4'},'Fontsize',12,'Location','Northeast');
  % hl = findobj(hobj,'type','line');
  % set(hl,'LineWidth',5);
   
   
   diffSamples = last - first; %finding the amount of samples each split happened for
   %TotalDiffSamples = [0,0,0,0];
   TotalDiffSamples = zeros(1, length(speakers));
   totalSamples = input(end,3); %this is the total amount of samples to be used for the percentage calculation
   inputSize = input(end,1); %gives the total count of splits from divideAudio
   
   for i=1:inputSize
        TotalDiffSamples(label(i)) = diffSamples(i) + TotalDiffSamples(label(i));
   end
   
   percentages = TotalDiffSamples / totalSamples;
   
   figure()
   person = {};
   for i=1:size(speakers)
   person{i,1} = ['Speaker ' num2str(i)];
   end
   pie(percentages,person)
   title(["Speaker percentages in " tester], 'Interpreter', 'none')
   
end

%IRRELEVANT
% function [out] = isin(input, point1, point2)
% 
% if input > point1 && input < point2
%     out = true;
% else
%     out = false;
% end
% end
