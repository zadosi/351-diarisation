clear
clc

%% 0 Pre-working

trainName = {'tommy_alone_2.WAV'; 'zach_alone_2.WAV'; 'grant_alone_2.WAV'; 'john_alone_2.WAV'};

%% 1 Feature Extraction
% Extract each piece's feature (MFCC and pitch)

feat = {};
for i = 1:length(trainName)
    feat{i, 1} = builtFeatureExtractor(trainName{i}, i);
end

%% 2 Machine Learning process to get the overall feature

[aud, fs] = audioread('short_conference.WAV');
[unknown] = builtFeatureExtractor('short_conference.WAV', -1);
tinfo = [feat{1}; feat{2}; feat{3}; feat{4}];

%Just getting rid of the empty space
% unknown(any(isnan(unknown), 2), :) = [];

%Seeing which k value returns the best result
result3 = [];
for i = 1:size(unknown,1)
    result3(i) = nearestneighbor(unknown(i, :), 3, tinfo);
end


%% 3 Conference 'Blank' Indentification and Slicing
% Slice the conference into pieces by identifying the blank length'

[div_cell, begin, end_n, fs] = divideAudio('short_conference.WAV', 0.01, 0.2, 1);


%% 4 Slice Feature Extraction and Matching
% Extract each slice of speech's feature. 

numslices = length(begin);
match = [];
for i = 1:numslices
    
    input = strcat('test', int2str(i), '.WAV');
    match(i) = matcher(input,tinfo);
%     stem(match);
%     pause();   
    
end



%% 5 Showing Result
% output a chart, showing the 

figure(1)
plot(result3, '.');
title('K = 3');


% match2 = [mode(result3(begin(1):end_n(1))) mode(result3(begin(2):end_n(2))) mode(result3(begin(3):end_n(3))) mode(result3(begin(4):end_n(4))) mode(result3(begin(5):end_n(5)))]; 

figure(2)
stem(match)



