function [divide_cell, divide_begin_time, divide_end_time, fs] = divideAudio(audioname,nsr, tol_time_slice, if_file)
%DIVIDEAUDIO Divide the audio by detedcting the silence
% a typical setting for reference:
% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% [div_cell, begin, end_n, fs] = divideAudio('short_conference.WAV', 0.01, 0.2, 1);
% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
IS_VOICE = 1; NOT_VOICE = 1;
[aud, fs] = audioread(audioname);
[~,n] = size(aud);
if n == 2
    stereo2mono(aud);
end
sile_thsd  = max(aud) * nsr;
tol_n_slice = floor(tol_time_slice * fs);
Voice_Spike_determine_time_period = 0.05;
n_it = 1;
i = 1;
while n_it < length(aud)
    while(NOT_VOICE)
        n_it = n_it + getTheTimeStart(aud(n_it:end), sile_thsd);
        if (isVoiceOrJustSpikes(aud, fs, n_it, sile_thsd, ...
                Voice_Spike_determine_time_period) == IS_VOICE)
            break;
        elseif n_it > length(aud)
            break;
        else
            n_it = n_it + floor(Voice_Spike_determine_time_period * fs);
        end
    end
    if n_it >= length(aud)
        break;
    end
    [divide_cell{i, 1}] = getAChunkOut(aud(n_it:end), sile_thsd, tol_n_slice);
    divide_begin_time(i) = n_it;
    n_it = n_it + length(divide_cell{i, 1});
    divide_end_time(i) = n_it;
    i = i + 1;
end
if if_file ~= 0
    for i = 1: length(divide_cell)
        audiowrite(['test' num2str(i) '.wav'], divide_cell{i, 1}, fs);
    end
end
end

function [n] = getTheTimeStart(aud, sil_thrd)
temp = 1;
while temp < length(aud)
    if aud(temp) > sil_thrd
        break;
    end
    temp = temp +1;
end
n = temp;
end

function [aud_d] = getAChunkOut(aud, sile_thsd, tol_n_sil)
it = 1;
blank_count = 0;
while it ~= length(aud)
    if aud(it) < sile_thsd
        blank_count = blank_count + 1;
    else
        blank_count = 0;
    end
    if blank_count > tol_n_sil
        aud_d = aud(1:it);
        break;
    end
    it = it + 1;
end
if it == length(aud)
    aud_d = aud(1:it);
end
end

function [is_voice] = isVoiceOrJustSpikes(aud, fs, it, sile_thsd, detect_time)
threshod_per_for_voice_treated_as_voice = 0.6;
%%% calculate parameter
n_len = floor(fs * detect_time);
if n_len > length(aud(it:end))
    n_len = length(aud(it:end));
end
voice_count = 0;
for i = 0:n_len - 1
%     while(it + i > length(aud))
%     end
    if abs(aud(it + i)) > sile_thsd
        voice_count = voice_count + 1;
    end
end
if voice_count > threshod_per_for_voice_treated_as_voice * n_len
    is_voice = 1;
else
    is_voice = 0;
    fft(is_voice);
end
end
