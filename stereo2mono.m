function [audio_read] = stereo2mono(audio_read)
    y = audio_read(:, 1) + audio_read(:, 2); %sum(y, 2) also accomplishes this
    peakAmp = max(abs(y)); 
    y = y/peakAmp;
    %  check the L/R channels for orig. peak Amplitudes
    peakL = max(abs(audio_read(:, 1)));
    peakR = max(abs(audio_read(:, 2))); 
    maxPeak = max([peakL peakR]);
    %apply x's original peak amplitude to the normalized mono mixdown 
    y = y*maxPeak;
    audio_read = y;   
end

