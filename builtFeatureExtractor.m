function [fwl] = builtFeatureExtractor(file, speakerLabel)

[audio_read, fs] = audioread(file);

pwrThreshold = -50; % Frames with power below this threshold (in dB) are likely to be silence
freqThreshold = 1000; % Frames with zero crossing rate above this threshold (in Hz) are likely to be silence or unvoiced speech

% Audio data will be divided into frames of 30 ms with 75% overlap
frameTime = 30e-3; %CHANGED from 30 based on https://link.springer.com/content/pdf/10.1007%2F978-3-642-25020-0.pdf (pg 117)
samplesPerFrame = floor(frameTime*fs);
increment = floor(0.25*samplesPerFrame);
overlapLength = samplesPerFrame - increment;

% check if the audio file is stereo or mono
[m, n] = size(audio_read); %gives dimensions of array where n is the number of stereo channels
if n == 2
    y = audio_read(:, 1) + audio_read(:, 2); %sum(y, 2) also accomplishes this
    peakAmp = max(abs(y)); 
    y = y/peakAmp;
    %  check the L/R channels for orig. peak Amplitudes
    peakL = max(abs(audio_read(:, 1)));
    peakR = max(abs(audio_read(:, 2))); 
    maxPeak = max([peakL peakR]);
    %apply x's original peak amplitude to the normalized mono mixdown 
    y = y*maxPeak;
    audio_read = y;   
end


[pitch1,~] = pitch(audio_read,fs, 'WindowLength',samplesPerFrame, 'OverlapLength',overlapLength);

mfcc1 = mfcc(audio_read,fs,'WindowLength',samplesPerFrame, ...
    'OverlapLength',overlapLength, 'LogEnergy', 'Replace');

voicing = detectVoicedFrame(audio_read, fs, frameTime, 25, pwrThreshold, freqThreshold);

% pitch1(voicing == 0) = nan;
% mfcc1(voicing == 0,:) = nan;


pitch1(voicing == 0) = 100;
mfcc1(voicing == 0,:) = 100;

% features = [mfcc1 pitch1];
features = [mfcc1];
label = ones(1, size(features,1))'.*speakerLabel;
fwl = [features label]; %insert the pitch in the last spot of each vector

    for i = 1:size(fwl,1)
        if((sum(features(i,:)))>100)
            fwl(i, end) = 0;
        else
            features(i,end-1) = (features(i,end-1) - mean(features(i,end-1)))./std(features(i,end-1));
        end
    end
end

