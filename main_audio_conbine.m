%%% combine several audio pieces into a long one
clc; clear;
fs = 48000;
fs_2 = 16000;
sil_let = zeros(0.08 * fs, 1);%% 0.08s silent
audio_tommy = stereo2mono(audioread("tommy_alone_2.WAV"));
audio_zach = stereo2mono(audioread("zach_alone_2.WAV"));
audio_john = stereo2mono(audioread("john_alone_2.WAV"));
audio_grant = stereo2mono(audioread("grant_alone_2.WAV"));
audio_short = stereo2mono(audioread("short_conference.WAV"));
audio_long = stereo2mono(audioread("long_conference.WAV"));
audio_yan = stereo2mono(audioread("yan.wav"));
audio_yan_1 = stereo2mono(audioread("yan_fox.wav"));
audio_yan_2 = stereo2mono(audioread("yan_gam.wav"));

audio_ran1 = [audio_john;sil_let;audio_grant; sil_let; audio_tommy];
audiowrite('ran1.wav', audio_ran1, fs);
audio_ran2 = [audio_tommy; sil_let; audio_zach; sil_let; audio_short;sil_let; audio_long];
audiowrite('ran2.wav', audio_ran2, fs);
audio_ran2 = [audio_yan; sil_let; audio_yan_1; sil_let; audio_yan_2];
audiowrite('ran_yan.wav', audio_ran2, fs_2);
disp('Finish!');