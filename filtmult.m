function [coeff] = filtmult(input, fs)
    
%Filter Banks
m = linspace(401.25, 2834.99, 15); %13 filter banks
h = 700.*(exp(m./1125)-1); %in Hz
f = floor(1024.*h./fs);
bank = zeros(13, 1024);

for i = 1:13
    p1 = f(i);
    p2 = f(i+1);
    p3 = f(i+2);
    
    bank(i,p1:p2) = linspace(0,1, p2-p1+1);
    bank(i,p2:p3) = linspace(1,0, p3-p2+1);    
end

coeff = bank*input;
    
end

