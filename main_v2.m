%% 0 Pre-working

trainName = {'tommy_alone_2.WAV'; 'zach_alone_2.WAV'; 'grant_alone_2.WAV'; 'john_alone_2.WAV'};

%% 1 Feature Extraction
% Extract each piece's feature (MFCC and pitch)

feat = {};
for i = 1:length(trainName)
    feat{i, 1} = builtFeatureExtractor(trainName{i}, i);
end


%% 2 Machine Learning process to get the overall feature

unknown = builtFeatureExtractor('short_conference.WAV', -1);
tinfo = [feat{1}; feat{2}; feat{3}; feat{4}];

%Just getting rid of the empty space
unknown(any(isnan(unknown), 2), :) = [];


%Seeing which k value returns the best result
result3 = [];
for i = 1:size(unknown,1)
    result3(i) = nearestneighbor(unknown(i, :), 3, tinfo);
end

result4 = [];
for i = 1:size(unknown,1)
    result4(i) = nearestneighbor(unknown(i, :), 4, tinfo);
end

result5 = [];
for i = 1:size(unknown,1)
    result5(i) = nearestneighbor(unknown(i, :), 5, tinfo);
end



%% 3 Conference 'Blank' Indentification and Slicing
% Slice the conference into pieces by identifying the blank length'

% [conference, fsc] = audioread('short_conference.WAV');
% [audio_cell] = divideAudioAccordToVoicedFlag(conference, fsc, 30e-3, voiced_flag, 25, tolerance, if_to_file)


%% 4 Slice Feature Extraction
% Extract each slice of speech's feature. 

%% 5 Slice Speeker Matching
% Match with the stored feature, and identify the speeker by the matching
% result of k-NN


%% 6 Showing Result
% output a chart, showing the 

figure(1)
stem(result3);
title('K = 3');
figure(2)
stem(result4);
title('K = 4');
figure(3)
stem(result5)
title('K = 5');


%taking mode of the best looking graph and trying to split where it looks
%like speaker changes
results = [mode(result3(1:297)), mode(result3(298:732)), mode(result3(733: 864)), mode(result3(985:end))];
figure(4)
stem(results);

