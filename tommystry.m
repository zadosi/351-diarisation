% clear
% clc
[audioIn, fs] = audioread('test.wav');
audio = audioIn(:,1)';
% twoStart = 110e3;
% twoStop = 135e3;
% audioIn = audioIn(twoStart:twoStop);
% timeVector = linspace((twoStart/fs),(twoStop/fs),numel(audioIn));

t = linspace(0, length(audio)/fs, length(audio));

figure(1)
plot(t,audio)
% axis([(twoStart/fs) (twoStop/fs) -1 1])
ylabel('Amplitude')
xlabel('Time (s)')
title('Utterance - Test')
% sound(audioIn,fs)

p = pitch(audioIn, fs);
windowLength = fs*20e-3;
c = mfcc(audioIn, fs);

%feature extraction:
coeff = c(3:end, :, 1);
coeff_avg = sum(coeff)/146;

figure(2)
stem(coeff_avg);
xlabel('Num of Coeff');
ylabel('Amplitude');
title('MFCCs of Utterance, averaged');

pitch_avg = sum(p(:, 1))/length(p(:, 1));
