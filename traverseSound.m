function [audio] = traverseSound(com_cell,fs, trainLabel)
%TRAVERSESOUND 此处显示有关此函数的摘要
%   此处显示详细说明
[cell_length, ~] = size(com_cell);
for i = 1:cell_length
    audio = cell2mat(com_cell(i, 2));
    sound(audio, fs);
    figure
    time = (1:length(audio))/fs;
    plot(time, audio);
    pause;
end
end

