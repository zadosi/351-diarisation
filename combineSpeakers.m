function [com_cell] = combineSpeakers(div_cell, match)
%CONBINESPEAKERS
% com_Spk = combineSpeakers(div_cell, match);
% audio_1 = cell2mat(com_Spk(1, 2));
% audio_2 = cell2mat(com_Spk(2, 2));
% audio_3 = cell2mat(com_Spk(3, 2));
% audio_4 = cell2mat(com_Spk(4, 2));
speaker = sort(unique(match));
com_cell = cell(length(speaker), 2);
for i = 1: length(speaker)
    com_cell(i, 1) = {speaker(i)};    
end
speaker_array = cell2mat(com_cell(:, 1));
[len_input, ~] = size(cell2mat(div_cell(i)));
if len_input ~= 1
    for i = 1: length(match)
    com_cell(speaker_array == match(i), 2) = ...
        {[cell2mat(com_cell(find(speaker_array == match(i)), 2)),(cell2mat(div_cell(i)))']};
    end
else
    for i = 1: length(match)
    com_cell(speaker_array == match(i), 2) = ...
        {[cell2mat(com_cell(find(speaker_array == match(i)), 2));(cell2mat(div_cell(i)))']};
    end
end

